import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:round1/login.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Short Project'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  int _selectedIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          centerTitle: true,
        ),
        bottomNavigationBar: bottomNavigationBar(),
        body: _selectedIndex == 0?
        HomePage():ApartmentPage()// This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  Widget bottomNavigationBar()
  {
    return new BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
            activeIcon: Icon(Icons.home_outlined),
            icon: Icon(Icons.home),
            label: "Home"),
        BottomNavigationBarItem(
            activeIcon: Icon(Icons.apartment),
            icon: Icon(Icons.apartment),
            label: "Apartment"),
      ],
      currentIndex: _selectedIndex,
      onTap: _onItemTapped,
    );
  }

  void _onItemTapped(int index)
  {
    setState(() {
      _selectedIndex = index;
    });
  }
}

class ApartmentPage extends StatelessWidget{
  @override
  Widget build(BuildContext context)
  {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(Icons.apartment, size: 40.0,),
          Padding(
            padding: const EdgeInsets.only(top: 20.0),
            child: MaterialButton(
              color: Colors.blue,
              onPressed:()
              {
                Navigator.push(context, CupertinoPageRoute(builder: (context)=>
                    Login()));
              },
              child: Text("Continue", style: TextStyle(color: Colors.white),),
            ),
          )
        ],
      ),
    );
  }
}

class HomePage extends StatelessWidget
{
  @override
  Widget build(BuildContext context)
  {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(Icons.home,size: 40.0,),
          Padding(
            padding: const EdgeInsets.only(top: 20.0),
            child: MaterialButton(
              color: Colors.blue,
              onPressed:()
              {
                Navigator.push(context, CupertinoPageRoute(builder: (context)=>
                    Login()));
              },
              child: Text("Continue", style: TextStyle(color: Colors.white),),
            ),
          )
        ],
      ),
    );
  }
}
