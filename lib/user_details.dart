import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class UserDetails extends StatefulWidget
{
  String name;
  String mobileNumber;
  String email;
  UserDetails({this.name, this.mobileNumber, this.email}):super();
  @override
  UserDetailsState createState() => UserDetailsState();
}

class UserDetailsState extends State<UserDetails>
{
  String name ="";
  String mobileNumber ="";
  String email ="";

  @override
  void initState()
  {
    super.initState();
    name = widget.name;
    mobileNumber = widget.mobileNumber;
    email = widget.email;
  }
  @override
  Widget build(BuildContext context)
  {
    return Scaffold(
      appBar: AppBar(
        title: Text("User Details", style: TextStyle(color: Colors.white),),
        centerTitle: true,
      ),
      body: Center(
        child: Container(
          margin: EdgeInsets.all(20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              name ==""?
              Container():
              CircleAvatar(
                child: Text('${widget.name[0].toUpperCase()}',
                  style: TextStyle(color: Colors.white),),
                radius: 30.0,
                backgroundColor: Colors.green,
              ),
              SizedBox(height: name==""?0.0:20.0,),
              Text(name, maxLines: 2,),
              SizedBox(height: name==""?0.0:20.0,),
              Text(email, maxLines: 2,),
              SizedBox(height: email==""?0.0:20.0),
              Text(mobileNumber),
              SizedBox(height: mobileNumber==""?0.0:20.0),
              name == ""?
              Container(
                child: Text("User Details Deleted Successfully"),
              ):
              RaisedButton(
                color: Colors.blue,
                child: Text("Delete", style: TextStyle(color: Colors.white),),
                onPressed: ()
                {
                  setState(() {
                    name ="";
                    email ="";
                    mobileNumber = "";
                  });
                },
              )
            ],
          ),
        ),
      ),
      //),
    );
  }
}