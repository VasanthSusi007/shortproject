import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:round1/user_details.dart';

class Login extends StatefulWidget
{
  @override
  LoginState createState() => LoginState();
}

class LoginState extends State<Login>
{
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _mobileController = new TextEditingController();
  TextEditingController _nameController = new TextEditingController();
  String _email;
  String _name;
  String _mobile;
  @override
  void dispose()
  {
    _nameController.dispose();
    _emailController.dispose();
    _mobileController.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context)
  {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Login", style: TextStyle(color: Colors.white),),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.all(20.0),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                nameField(),
                contactNumberField(),
                emailField(),
                SizedBox(height: 30.0,),
                submitButton()
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget nameField()
  {
    return TextFormField(
      keyboardType: TextInputType.text,
      controller: _nameController,
      decoration: InputDecoration(
          hintStyle: TextStyle(color: Colors.grey, height: 2),
          labelStyle: TextStyle(color: Colors.black),
          contentPadding: EdgeInsets.fromLTRB(0, 20.0, 0, 7),
          labelText: 'Name',
          hintText: 'enter your name'),
      validator: (value) => value.isEmpty
          ? 'Name cannot be blank' : null,
      onSaved: (value) => _name = value,
    );
  }

  Widget contactNumberField()
  {
    return TextFormField(
      keyboardType: TextInputType.phone,
      controller: _mobileController,
      maxLength: 10,
      decoration: InputDecoration(
          counterText: "",
          hintStyle: TextStyle(color: Colors.grey, height: 2),
          labelStyle: TextStyle(color: Colors.black),
          contentPadding: EdgeInsets.fromLTRB(0, 20.0, 0, 7),
          labelText: 'Contact Number',
          hintText: 'enter your mobile number'),
      validator: (value) => value.isEmpty
          ? 'Contact Number cannot be blank' : value.length != 10?
      'Mobile number must have 10 digits':null,
      onSaved: (value) => _mobile = value,
    );
  }

  Widget emailField()
  {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      controller: _emailController,
      decoration: InputDecoration(
          hintStyle: TextStyle(color: Colors.grey, height: 2),
          labelStyle: TextStyle(color: Colors.black),
          contentPadding: EdgeInsets.fromLTRB(0, 20.0, 0, 7),
          labelText: 'Email',
          hintText: 'enter your email'),
      validator: (value) => value.isEmpty
          ? 'Email cannot be blank'
          : !value.contains('@') ? 'Invalid Email' : null,
      onSaved: (value) => _email = value,
    );
  }

  Widget submitButton() {
    return SizedBox(
      width: double.infinity,
      child: RaisedButton(
          color: Color.fromRGBO(54, 197, 101, 1),
          child: Text(
            "Submit",
            style: TextStyle(color: Colors.white),
          ),
          onPressed: ()
          {
            final form = _formKey.currentState;
            if(form.validate())
            {
              form.save();
              Navigator.push(context, CupertinoPageRoute(builder: (context)=>
                  UserDetails(
                    email: _email,
                    name: _name,
                    mobileNumber: _mobile,
                  )));
            }
          }),
    );
  }
}